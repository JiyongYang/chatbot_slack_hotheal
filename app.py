import re
import json

from flask import Flask, request

from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes.interactions import MessageInteractiveEvent

import Parsing_module

SLACK_TOKEN = "xoxb-686562110308-677176448707-CxYBJ6r85AIkq0ey4QIBlJDr"
SLACK_SIGNING_SECRET = "89e9bdfb86fb67d4c3d1c02ca368bae4"

app = Flask(__name__)
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

check_pre_time = None

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    global check_pre_time

    _channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    event_time = event_data["event_time"]

    if check_pre_time == event_time:
        return None
    check_pre_time = event_time

    result = Parsing_module._detect_input(text)

    if type(result) != tuple:
        slack_web_client.chat_postMessage(
            channel=_channel,
            text="base",
            blocks=result
        )
    else:
        if (result[0]):
            item_name_filter = re.compile(r'\(.+\)')
            item_name_list = item_name_filter.findall(text)

            slack_web_client.chat_postMessage(
                channel=_channel,
                attachments=[
                    {
                        "title": "선택하신 제품과 가장 유사한 제품의 가격변동 추이에요.",
                        "color": "#2A3145"
                    },
                    {
                        "text": "\n\n"
                    },
                    {
                        "text": "입력한 제품: \t{0}\n".format(item_name_list[0].strip(')').strip('(')),
                        "color": "#524FA1"
                    },
                    {
                        "text": "검색된 제품: \t<{0}|{1}>\n".format(result[4], result[1]),
                        "color": "#51BEBB"
                    },
                    {
                        "text": "최저가: \t\t\t{0}\n".format(result[3]),
                        "color": "#EC5067"
                    }
                ]
            )
            slack_web_client.files_upload(channels=_channel, file=result[2])
        elif(result[0] == False and result[1] == "help"):
            slack_web_client.chat_postMessage(
                channel=_channel,
                attachments=result[2]
            )
        else:
            slack_web_client.chat_postMessage(
                channel=_channel,
                attachments=[
                    {
                        "title": "요청하신 [" + result[1] + "] 제품에 대한 가격변동정보가 존재하지 않아요.",
                        "color": "#C00000"
                    }
                ]
            )



@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    item_name = click_event.value
    result = Parsing_module._get_history_of_price(item_name)


    if (result[0]):
        slack_web_client.chat_postMessage(
            channel=click_event.channel.id,
            attachments=[
                {
                    "title": "선택하신 제품과 가장 유사한 제품의 가격변동 추이에요.",
                    "color": "#2A3145"
                },
                {
                    "text": "\n\n"
                },
                {
                    "text": "입력한 제품: \t{0}\n".format(item_name),
                    "color": "#524FA1"
                },
                {
                    "text": "검색된 제품: \t<{0}|{1}>\n".format(result[4], result[1]),
                    "color": "#51BEBB"
                },
                {
                    "text": "최저가: \t\t\t{0}\n".format(result[3]),
                    "color": "#EC5067"
                }
            ]
        )
        slack_web_client.files_upload(channels=click_event.channel.id, file=result[2])
    else:
        slack_web_client.chat_postMessage(
            channel=click_event.channel.id,
            attachments=[
                {
                    "title": "요청하신 [" + result[1] + "] 제품에 대한 가격변동정보가 존재하지 않아요.",
                    "color": "#C00000"
                }
            ]
        )
    return "OK", 200



# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=4040)

