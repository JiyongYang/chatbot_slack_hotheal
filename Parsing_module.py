import json
import re

from collections import OrderedDict

import urllib.request
import urllib.parse
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import matplotlib.pyplot as plt
import matplotlib.font_manager as fm

def _json_parse(itemlist, rank=False):

    jsonList = []
    seletion_list = []

    file_data = OrderedDict()
    file_data["type"] = "section"
    file_data["text"] = {
        "type": "mrkdwn",
        "text": "\n요청하신 " + str(len(itemlist)) + "개의 핫딜입니다.\n\n *아이템 목록:*"
    }
    jsonList.append(file_data)

    file_data = OrderedDict()
    file_data["type"] = "divider"
    jsonList.append(file_data)
    file_data = OrderedDict()
    file_data["type"] = "divider"
    jsonList.append(file_data)

    for i in range(len(itemlist)):

        # for selection panel
        seletion_item = OrderedDict()

        seletion_item["text"] = {
            "type": "plain_text",
            "text": itemlist[i][0] if len(itemlist[i][0]) < 30 else itemlist[i][0][:30],
        }
        seletion_item["value"] = itemlist[i][0] if len(itemlist[i][0]) < 50 else itemlist[i][0][:50]
        seletion_list.append(seletion_item)

        file_data = OrderedDict()
        file_data["type"] = "actions"
        file_data["elements"] = [
            {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": itemlist[i][4],
                    "emoji": True
                },
                "value": "None",
                "style": "primary"
            },
            {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": itemlist[i][5],
                    "emoji": True
                },
                "value": itemlist[i][6],
                "style": "danger"
            },
            {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": ":edgar_time: " + itemlist[i][2],
                    "emoji": True
                },
                "value": "None",
            }
        ]
        jsonList.append(file_data)

        file_data = OrderedDict()
        file_data["type"] = "section"
        file_data["text"] = {
            "type": "mrkdwn",
            "text": str("#*"+ str(i+1) + "*  " if rank == True else "") +
                    "*<" + itemlist[i][6] + "|" + itemlist[i][0] + ">*\n" +
                    str(itemlist[i][1] if itemlist[i][1] != "None" else "가격정보없음") + "\n" +
                    str(":comment: " + itemlist[i][7] + "  " if itemlist[i][7] != "None" else "") +
                    str(":edgar_up: " + itemlist[i][8] + "  " if itemlist[i][8] != "None" else "") +
                    str(":edgar_down: " + itemlist[i][9] + "  " if itemlist[i][9] != "None" else "") + "\n"

        }
        # file_data["url"] = itemlist[i][5]
        if (itemlist[i][3] != "None"):
            file_data["accessory"] = {
                "type": "image",
                "image_url": itemlist[i][3],
                "alt_text": "이미지 파일\n X",
            }
        else:
            file_data["accessory"] = {
                "type": "image",
                "image_url": "https://static.thenounproject.com/png/340719-200.png",
                "alt_text": "이미지 파일\n X"
            }
        jsonList.append(file_data)

        file_data = OrderedDict()
        file_data["type"] = "divider"
        jsonList.append(file_data)

    file_data = OrderedDict()
    file_data["type"] = "divider"
    jsonList.append(file_data)

    file_data = OrderedDict()
    file_data["type"] = "section"
    file_data["text"] = {
        "type": "mrkdwn",
        "text": "최저가 변동 추이를 보고 싶은 핫딜을 선택하세요."
    }

    file_data["accessory"] = {
        "type": "static_select",
        "placeholder": {
            "type": "plain_text",
            "text": "Select an item",
            "emoji": True
        },
        "options": seletion_list
    }
    jsonList.append(file_data)

    result = json.dumps(jsonList, ensure_ascii=False, indent="\t")
    return json.loads(result)

def _crawl_listitem(cnt, rank=False):
    url = "https://algumon.com"
    if rank:
        url = "https://algumon.com/deal/rank"

    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    list_items = []

    # 입력 숫자만큼 리스트에 추가
    index_cnt = int(cnt)

    if index_cnt < 1:
        index_cnt = 5
    elif index_cnt > 15:
        index_cnt = 15

    for list_item in soup.find_all("div", class_="post-group"):
        if index_cnt == 0:
            break
        index_cnt -= 1

        # product name
        product_name = list_item.find("a", class_="product-link").getText().strip()

        # price
        price_tag = list_item.find("small", class_="product-price")
        if price_tag != None:
            price = price_tag.getText().strip()
        else:
            price = "None"

        # time
        time = list_item.find("small", class_="label-time pull-right text-muted").getText().strip()

        # img_url
        img_url_tag = list_item.find("div", class_="product-img-box")
        is_img = img_url_tag.find("span", class_="no-image")
        if is_img == None:
            str_img = img_url_tag.find("img").get("src")
            img_url = "https://" + urllib.parse.quote(str_img[8:], safe="/")
        else:
            img_url = "https://static.thenounproject.com/png/340719-200.png"

        # market_str
        market_str = list_item.find("span", class_="label shop").getText().strip()

        # source_str
        source_str = list_item.find("span", class_="label site").getText().strip()

        # source_url
        source_url = url + list_item.find("a", class_="product-link").get("href")

        # comment_cnt, up_cnt, down_cnt
        cnt_trigger = [0, 0, 0]
        comment_tag = list_item.find_all("small", class_="text-muted")
        is_comment = comment_tag[1].find("i", class_="fa fa-commenting-o fa-fw")
        is_up = comment_tag[1].find("i", class_="fa fa-thumbs-o-up fa-fw")
        is_down = comment_tag[1].find("i", class_="fa fa-thumbs-o-down fa-fw")

        cnt_list = comment_tag[1].getText().split()
        # result [comment , up, down]
        result_cnt = []
        if is_comment == None:
            cnt_trigger[0] = 0
            result_cnt.append("None")
        else:
            cnt_trigger[0] = 1
            result_cnt.append(cnt_list[0])
            cnt_list.pop(0)
        if is_up == None:
            cnt_trigger[1] = 0
            result_cnt.append("None")
        else:
            cnt_trigger[1] = 1
            result_cnt.append(cnt_list[0])
            cnt_list.pop(0)
        if is_down == None:
            cnt_trigger[2] = 0
            result_cnt.append("None")
        else:
            cnt_trigger[2] = 1
            result_cnt.append(cnt_list[0])
            cnt_list.pop(0)

        comment_cnt = result_cnt[0]
        up_cnt = result_cnt[1]
        down_cnt = result_cnt[2]

        # list_tuple(이름, 가격, 시간, 이미지url, market str, source str, source url, comment-cnt, up-cnt,down-cnt)
        list_tuple = (
        product_name, price, time, img_url, market_str, source_str, source_url, comment_cnt, up_cnt, down_cnt)
        list_items.append(list_tuple)

    if index_cnt != 0:
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        post_item = soup.find("li", class_="left clearfix post-li").get("data-post-id")
        post_url = "https://algumon.com/more/1?types=ended&topSequence=" + str(post_item)
        source_code = urllib.request.urlopen(post_url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        for list_item in soup.find_all("div", class_="post-group"):
            if index_cnt == 0:
                break
            index_cnt -= 1

            # product name
            product_name = list_item.find("a", class_="product-link").get("data-label").strip()

            # price
            price_tag = list_item.find("small", class_="product-price")
            if price_tag != None:
                price = price_tag.getText().strip()
            else:
                price = "None"

            # time
            time = list_item.find("small",
                                  class_="label-time pull-right text-muted").getText().strip()

            # img_url
            img_url_tag = list_item.find("div", class_="product-img-box")
            is_img = img_url_tag.find("span", class_="no-image")
            if is_img == None:
                str_img = img_url_tag.find("img").get("src")
                img_url = "https://" + urllib.parse.quote(str_img[8:], safe="/")
            else:
                img_url = "https://static.thenounproject.com/png/340719-200.png"

            # market_str
            market_str = list_item.find("span", class_="label shop").getText().strip()

            # source_str
            source_str = list_item.find("span", class_="label site").getText().strip()

            # source_url
            source_url = url + list_item.find("a", class_="product-link").get("href")

            # comment_cnt, up_cnt, down_cnt
            cnt_trigger = [0, 0, 0]
            comment_tag = list_item.find_all("small", class_="text-muted")
            is_comment = comment_tag[1].find("i", class_="fa fa-commenting-o fa-fw")
            is_up = comment_tag[1].find("i", class_="fa fa-thumbs-o-up fa-fw")
            is_down = comment_tag[1].find("i", class_="fa fa-thumbs-o-down fa-fw")

            cnt_list = comment_tag[1].getText().split()
            # result [comment , up, down]
            result_cnt = []
            if is_comment == None:
                cnt_trigger[0] = 0
                result_cnt.append("None")
            else:
                cnt_trigger[0] = 1
                result_cnt.append(cnt_list[0])
                cnt_list.pop(0)
            if is_up == None:
                cnt_trigger[1] = 0
                result_cnt.append("None")
            else:
                cnt_trigger[1] = 1
                result_cnt.append(cnt_list[0])
                cnt_list.pop(0)
            if is_down == None:
                cnt_trigger[2] = 0
                result_cnt.append("None")
            else:
                cnt_trigger[2] = 1
                result_cnt.append(cnt_list[0])
                cnt_list.pop(0)

            comment_cnt = result_cnt[0]
            up_cnt = result_cnt[1]
            down_cnt = result_cnt[2]

            # list_tuple(이름, 가격, 시간, 이미지url, market str, source str, source url, comment-cnt,
            # up-cnt,down-cnt)
            list_tuple = (
                product_name, price, time, img_url, market_str, source_str, source_url, comment_cnt,
                up_cnt, down_cnt)
            list_items.append(list_tuple)

    return list_items

def _detect_input(text):
    compile_txt = re.compile(r'\d+')
    digit_txt_list = compile_txt.findall(text)

    if (re.search('핫딜', text) and (re.search('인기', text)) or re.search('랭크', text)):
        if len(digit_txt_list) == 1:
            result = _json_parse(_crawl_listitem(int(digit_txt_list[0]), rank=True), rank=True)
        else:
            result = _json_parse(_crawl_listitem(5, rank=True), rank=True)
        return result
    # elif re.search('추천', text) or re.search('살만한', text):
    #     return "살만한 물건 추천"
    elif re.search('변화', text) or re.search('변동', text):
        item_name_filter = re.compile(r'\(.+\)')
        item_name_list = item_name_filter.findall(text)
        return _get_history_of_price(item_name_list[0].strip(')').strip('('))
    elif re.search('도움', text) or re.search('help', text) or re.search('옵션', text) or re.search('option', text):
        attachment = [
            {
                "title": "핫딜 봇을 사용하기 위한 도움말",
                "text": "\n 현재 가장 핫한 제품을 얻기 위해 HotDealBot을 호출해보세요.\n",
                "color": "#2A3145"
            },
            {
                "text": "\n\n"
            },
            {
                "text": "\n\n"
            },
            {
                "text": "현재 핫딜 제품 리스트 보기",
                "color": "#524FA1"
            },
            {
                "text": "오늘 핫딜 제품 중 가장 인기가 많은 제품 리스트 보기",
                "color": "#51BEBB"
            },
            {
                "text": "특정 제품의 6개월 간 최저가 변동 그래프 보기\n ※ 주의: 제품명은 (...)안에 넣어어 해요.",
                "color": "#EC5067"
            }
        ]
        return (False, "help", attachment)
    else:
        if len(digit_txt_list) == 1:
            print(int(digit_txt_list[0]))
            result = _json_parse(_crawl_listitem(int(digit_txt_list[0])))
        else:
            result = _json_parse(_crawl_listitem(int(5)))
        return result

    print("Not working")

def _get_history_of_price(item_name):
    selected_item_name = ""

    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument('window-size=1920x1080')
    options.add_argument("disable-gpu")

    driver = webdriver.Chrome(chrome_options=options)
    driver.get("https://shopping.naver.com/")
    elem = driver.find_element_by_name("query")
    elem.send_keys(item_name)
    elem.send_keys(Keys.RETURN)
    driver.implicitly_wait(10)

    try:
        grouped_item = driver.find_element_by_xpath("//li[@class='_model_list _itemSection']")
    except Exception as e:
        # print(e)
        error_msg = u"요청하신 [" + item_name + u"]에 대한 가격 변동 정보가 존재하지 않아요.ㅠㅠ"
        print(driver.current_url)
        return (False, item_name)
    else:
        if grouped_item is not None:
            selected_item_name = driver.find_element_by_xpath(
                "//li[@class='_model_list _itemSection']/div/a[@class='tit']").text

            elem = driver.find_element_by_xpath("//li[@class='_model_list _itemSection']/div/a[@class='tit']")
            elem.click()

            url = elem.get_attribute('href')

            source_code = urllib.request.urlopen(url).read().decode('utf-8')
            soup = BeautifulSoup(source_code, "html.parser")

            item_price = soup.find("span", class_="low_price").get_text()


            pos1 = source_code.find("priceChartHandler=shop.detail.HistoryChartHandler")
            pos2 = source_code.find("var countChartHandler=shop.detail.HistoryChartHandler")
            mod_source_code = source_code[pos1:pos2]

            price_list = mod_source_code.split(',')[2].split('|')
            price_list = list(map(lambda x: int(x.replace('"', "").replace(';', "").replace(')', "")), price_list))

            date_list = mod_source_code.split(',')[1].split('|')
            date_list = list(map(lambda x: float(x.replace('"', "").replace(';', "").replace(')', "")), date_list))

            six_month_row_price = []
            for i in range(13):
                six_month_row_price.append((date_list[35 + i * 12], price_list[35 + i * 12]))


            graph_plot(six_month_row_price, selected_item_name)
        return (True, selected_item_name, 'plot.png', item_price.strip(), url)

def graph_plot(raw_data, item_name):
    pos = range(len(raw_data))

    font = fm.FontProperties(fname='./NanumBarunGothic.ttf')
    plt.title("[" + str(item_name) + "]의 최근 6개월 간 최저가", fontproperties=font)
    plt.plot(pos, [data[1] for data in raw_data], 'rs--')
    plt.xticks(pos, [str(data[0]) for data in raw_data], fontproperties=font)
    plt.xlabel("날짜", fontproperties=font)
    plt.ylabel("가격", fontproperties=font)
    plt.tight_layout()
    plot_filename = 'plot.png'
    plt.savefig(plot_filename)

    plt.show()